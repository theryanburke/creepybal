/**
 * CreepyBAL
 * @author Ryan Burke
 * 
 * CreepyBAL is a hackathon submission for 2lemetry's 2014 Q1 'hacklessathon'
 *
 * @param options - JS Object
 * expecting the following format:
 * var options = {
 *   domain: "YOUR DOMAIN HERE", // your ThingFabric project domain
 *   credential: {
 *     username: "INSERT USERNAME", // a credential username
 *     password: "INSERT TOKEN" // a matching credential token
 *   },
 *   featuresEnabled: {
 *     click: true, // boolean to enable tracking of this event type
 *     mousemove: true,
 *     mouseover: true,
 *     monitor: false // is this a client or a monitor
 *   }
 * };
 *
 * You can send your users messages on the topic (domain)/users/(cid)/inbound
 */
function CreepyBAL(options) {

    var r = Math.round(Math.random()*Math.pow(10,5));
    var d = new Date().getTime();
    var cid = r.toString() + "-" + d.toString();

    var domain = options.domain; //project domain

    var clientOptions = {};
    clientOptions.userName = options.credential.username; //credential username
    clientOptions.password = options.credential.password; //credential password
    clientOptions.keepAliveInterval = 30;
    clientOptions.onSuccess = onConnect;
    clientOptions.onFailure = onConnectFailure;

    var client = new Messaging.Client("q.mq.tt", 8083, cid);
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;
    client.connect(clientOptions);

    function onConnect() {
      console.log("onConnect");
      $("p#status").html("Connected!");
      $("p#clientid").html("Client ID: " + cid);
      if(options.featuresEnabled.monitor) {
        client.subscribe(domain + "/users/+");
      } else {
        client.subscribe(domain + "/users/" + cid + "/inbound");
      }
    }
    function onConnectFailure() {
      console.log("failed to connect");
      $("p#status").html("Failed to connect! :(");
    }
    function onConnectionLost(responseObject) {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:"+responseObject.errorMessage);
        $("p#status").html("Connection lost :(");
      }
    }
    function onMessageArrived(message) {
      console.log("onMessageArrived:"+message.payloadString);
      if(options.featuresEnabled.monitor) {
        var payload = JSON.parse(message.payloadString);
        if(payload.id) {
          var $el = $("#" + payload.id);
          var border = $el.css("border");
          $el.css("backgroundColor", '#ddd');
          setTimeout(function() {
            $el.css("backgroundColor", '#fff');
          }, 1000);
        }
      } else {
        $("p#incoming").html("New message received from HQ: " + message.payloadString);
      }
    }  

    function sendMessage(body) {
      body = JSON.stringify(body);
      var message = new Messaging.Message(body);
      message.destinationName = domain + "/users/" + cid;
      client.send(message);
      $("p#status").html("Sent a message:" + body);
    }


    //creepybal meat

    if(options.featuresEnabled.mousemove) {
      $("body").mousemove(function(event) {
        var message = {
          event: "mousemove",
          pagex: event.pageX,
          pagey: event.pageY,
          clientx: event.clientX,
          clienty:event.clientY
        };
        sendMessage(message);
      });
    }
    
    if(options.featuresEnabled.click) {
      $("[id]").click(function() {
        var $element = $(this);
      
        var id = $element.attr("id");

        var message = {
          id: id,
          event: "click"
        };
        sendMessage(message);
      });
    }

    if(options.featuresEnabled.mouseover) {
      $("[id]").mouseover(function() {
        var $element = $(this);
      
        var id = $element.attr("id");

        var message = {
          id: id,
          event: "mouseover"
        };
        sendMessage(message);
      });
    }


}
